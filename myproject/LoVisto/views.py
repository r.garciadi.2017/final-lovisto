from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from urllib.parse import urlparse
from django.utils import timezone
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from .models import Aportacion, Perfil, Aemet, Dia, Youtube, Wikipedia, Otro, Comentario, Aportacion_votada
from .forms import AportacionForm, UserForm, ComentarioForm
from .parser import html_parser, html_parser2, TxtParser, ytb_js_handler, AemetParser


# Create your views here.
def analisis_sitio(url):
    parsed = urlparse(url)
    if (parsed.netloc == "www.aemet.es" or parsed.netloc == "aemet.es"):
        res = parsed.path.split('/')[-1]
        split = parsed.path.split('-')[0]
        if (split.startswith("/es/eltiempo/prediccion/municipios/")):
            municipio, id = res.split('-')
            id = id.split("id")[-1]
            if (municipio.isalpha() and id.isdigit()):
                return True

    elif (parsed.netloc == "www.youtube.com" or parsed.netloc == "youtube.com"):
        if (parsed.path == "/watch"):
            video = parsed.query.split("=")[0]
            if ("v" == video):
                return True

    elif (parsed.netloc == "es.wikipedia.org"):
        if (parsed.path.startswith("/wiki/")):
            return True

    return False

def last_aports():
    list_aports = []
    aports = Aportacion.objects.all()
    for aport in aports:
        list_aports.append(aport)

    list_aports.reverse()
    list_aports = list_aports[:10]
    return list_aports

def last_all_aports():
    list_aports = []
    aports = Aportacion.objects.all()
    for aport in aports:
        list_aports.append(aport)

    list_aports.reverse()
    return list_aports

def last_user(usu):
    list_user = []
    perfil = Perfil.objects.get(usuario=usu)
    aports = Aportacion.objects.filter(user=perfil)
    for aport in aports:
        list_user.append(aport)

    list_user.reverse()
    list_user = list_user[:5]
    return list_user

def last_sites():
    list_sites = []
    sites = Aportacion.objects.all()
    for site in sites:
        list_sites.append(site.url)

    list_sites.reverse()
    list_sites = list_sites[:3]
    return list_sites

def contar_voto(id, perfil, value):
    aportacion = Aportacion.objects.get(id=id)
    try:
        voto_user = Aportacion_votada.objects.filter(usuario=perfil).get(aportacion=aportacion)
        if voto_user.voto == 1 and value == "dislike":
            aportacion.votos_pos = aportacion.votos_pos - 1
            aportacion.votos_neg = aportacion.votos_neg + 1
            voto_user.delete()
            voto_user = Aportacion_votada(usuario=perfil, aportacion=aportacion, voto=-1)
            voto_user.save()
        elif voto_user.voto == -1 and value == "like":
            aportacion.votos_pos = aportacion.votos_pos + 1
            aportacion.votos_neg = aportacion.votos_neg - 1
            voto_user.delete()
            voto_user = Aportacion_votada(usuario=perfil, aportacion=aportacion, voto=1)
            voto_user.save()
        elif (voto_user.voto == 1 and value == "like"):
            aportacion.votos_pos = aportacion.votos_pos - 1
            voto_user.delete()
        elif (voto_user.voto == -1 and value == "dislike"):
            aportacion.votos_neg = aportacion.votos_neg - 1
            voto_user.delete()
    except Aportacion_votada.DoesNotExist:
        voto_user = Aportacion_votada(usuario=perfil, aportacion=aportacion, voto=0)
        if value == "like":
            voto_user.voto = 1
            aportacion.votos_pos = aportacion.votos_pos + 1
        elif value == "dislike":
            voto_user.voto = -1
            aportacion.votos_neg = aportacion.votos_neg + 1
        voto_user.save()
    aportacion.save()

@csrf_exempt
def index(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            perfil = Perfil.objects.get(usuario=request.user)
            if "aport" in request.POST:
                aportacion = Aportacion(user=perfil)
                aportacion.fecha = timezone.now()
                form = AportacionForm(request.POST, instance=aportacion)

                if form.is_valid():
                    f = form.save()
                    if (analisis_sitio(aportacion.url)):
                        if("aemet.es" in aportacion.url):
                            if(not ("www" in aportacion.url)):
                                url_org = aportacion.url
                                split = aportacion.url.split("//")
                                split.insert(1, "//www.")
                                aportacion.url = ''.join(split)
                                aportacion.save()
                                xml = html_parser(aportacion.url)
                                AemetParser(xml)
                                aportacion.url = url_org
                                aportacion.save()
                            else:
                                xml = html_parser(aportacion.url)
                                AemetParser(xml)

                        elif("youtube.com" in aportacion.url):
                            json = html_parser(aportacion.url)
                            ytb_js_handler(json, aportacion.url)

                        elif ("wikipedia.org" in aportacion.url):
                            articulo = html_parser(aportacion.url)
                            texto = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles={articulo}&prop=extracts&exintro&explaintext"
                            TxtParser(texto.replace("{articulo}", articulo))

                    else:
                        html_parser2(aportacion.url)

            elif "modo" in request.POST:
                if(perfil.modo == 'n'):
                    perfil.modo = 'o'
                else:
                    perfil.modo = 'n'
                perfil.save()
                return redirect("/")

            else:
                value = request.POST['submit']
                id = request.POST['aport_id']
                contar_voto(id, perfil, value)
                form = AportacionForm()

        else:
            form = AportacionForm()


    aportacion_list = last_aports()
    aemet_list = Aemet.objects.all()
    dia_list = Dia.objects.all()
    ytb_list = Youtube.objects.all()
    wiki_list = Wikipedia.objects.all()
    otro_list = Otro.objects.all()
    if request.user.is_authenticated:
        perfil = Perfil.objects.get(usuario=request.user)
        votos_list = Aportacion_votada.objects.filter(usuario=perfil)
    else:
        votos_list = None
        perfil = None
    list_sites = last_sites()
    context = {'aportacion_list': aportacion_list, 'aemet_list': aemet_list, 'dia_list': dia_list, 'ytb_list': ytb_list,
               'wiki_list': wiki_list, 'otro_list': otro_list, 'votos_list': votos_list, 'perfil': perfil, 'list_sites': list_sites}
    formato = request.GET.get('format')
    if formato == 'xml':
        return render(request, 'LoVisto/aportaciones.xml', context, content_type='text/xml')
    if formato == 'json':
        return render(request, 'LoVisto/aportaciones.json', context, content_type='text/json')
    if request.user.is_authenticated:
        user_list = last_user(request.user)
        context2 = {'form': form, 'user_list': user_list, 'usuario': request.user}
        context.update(context2)
    return render(request, 'LoVisto/inicio.html', context)

def logout_view(request):
    logout(request)
    return redirect("/")

def registro(request):
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.set_password(user.password)
            user.save()
            perfil = Perfil(usuario = user)
            perfil.save()
        return redirect("/")
        
    else:
        form = UserForm()
        list_sites = last_sites()
        context = {'form': form, 'list_sites': list_sites}
        return render(request, 'LoVisto/registro.html', context)

@csrf_exempt
def aportacion(request, llave):
    if request.user.is_authenticated:
        if request.method == "POST":
            perfil = Perfil.objects.get(usuario=request.user)
            if "coment" in request.POST:
                aport = Aportacion.objects.get(id=llave)
                form = ComentarioForm(request.POST)

                if form.is_valid():
                    f = form.save(commit=False)
                    f.usuario = perfil
                    f.aportacion = aport
                    f.fecha = timezone.now()
                    f.save()
                    aport.ncomentarios += 1
                    aport.save()

            elif "modo" in request.POST:
                if (perfil.modo == 'n'):
                    perfil.modo = 'o'
                else:
                    perfil.modo = 'n'
                perfil.save()
                return redirect("/" + str(llave))

            else:
                value = request.POST['submit']
                id = request.POST['aport_id']
                contar_voto(id, perfil, value)
                form = ComentarioForm()
        else:
            form = ComentarioForm()

    aportacion = Aportacion.objects.get(id=llave)
    try:
        aemet = Aemet.objects.get(aportacion=aportacion)
    except Aemet.DoesNotExist:
        aemet = None
    try:
        dia_list = Dia.objects.filter(aemet=aemet)
    except Dia.DoesNotExist:
        dia_list = None
    try:
        ytb = Youtube.objects.get(aportacion=aportacion)
    except Youtube.DoesNotExist:
        ytb = None
    try:
        wiki = Wikipedia.objects.get(aportacion=aportacion)
    except Wikipedia.DoesNotExist:
        wiki = None
    try:
        otro = Otro.objects.get(aportacion=aportacion)
    except Otro.DoesNotExist:
        otro = None
    try:
        comentarios = Comentario.objects.filter(aportacion=aportacion)
    except Comentario.DoesNotExist:
        comentarios = None
    if request.user.is_authenticated:
        perf = Perfil.objects.get(usuario=request.user)
        try:
            voto = Aportacion_votada.objects.get(usuario=perf, aportacion=aportacion)
        except Aportacion_votada.DoesNotExist:
            voto = None
    else:
        voto = None
        perf = None
    list_sites = last_sites()
    context = {'aportacion': aportacion, 'aemet': aemet, 'dia_list': dia_list, 'ytb': ytb,
               'wiki': wiki, 'otro': otro, 'comentarios': comentarios, 'voto': voto, 'perfil': perf,
               'list_sites': list_sites}
    formato = request.GET.get('format')
    if formato == 'xml':
        return render(request, 'LoVisto/aportacion.xml', context, content_type='text/xml')
    if formato == 'json':
        return render(request, 'LoVisto/aportacion.json', context, content_type='text/json')
    if request.user.is_authenticated:
        context2 = {'form': form}
        context.update(context2)
    return render(request, 'LoVisto/aportacion.html', context)

def todo(request):
    if request.user.is_authenticated:
        perfil = Perfil.objects.get(usuario=request.user)
        if request.method == "POST":
            if "modo" in request.POST:
                if (perfil.modo == 'n'):
                    perfil.modo = 'o'
                else:
                    perfil.modo = 'n'
                perfil.save()

    else:
        perfil = None
    aportacion_list = last_all_aports()
    aemet_list = Aemet.objects.all()
    dia_list = Dia.objects.all()
    ytb_list = Youtube.objects.all()
    wiki_list = Wikipedia.objects.all()
    otro_list = Otro.objects.all()
    list_sites = last_sites()
    coment_list = Comentario.objects.all()
    context = {'aportacion_list': aportacion_list, 'aemet_list': aemet_list, 'dia_list': dia_list, 'ytb_list': ytb_list,
               'wiki_list': wiki_list, 'otro_list': otro_list, 'perfil': perfil, 'list_sites': list_sites, 'coment_list': coment_list}
    formato = request.GET.get('format')
    if formato == 'xml':
        return render(request, 'LoVisto/comentarios.xml', context, content_type='text/xml')
    if formato == 'json':
        return render(request, 'LoVisto/comentarios.json', context, content_type='text/json')
    return render(request, 'LoVisto/todo.html', context)

def usuario(request):
    if request.user.is_authenticated:
        perfil = Perfil.objects.get(usuario=request.user)
        if request.method == "POST":
            perfil = Perfil.objects.get(usuario=request.user)
            if "modo" in request.POST:
                if (perfil.modo == 'n'):
                    perfil.modo = 'o'
                else:
                    perfil.modo = 'n'
                perfil.save()

        aportacion_list = Aportacion.objects.filter(user=perfil)
        comentario_list = Comentario.objects.filter(usuario=perfil)
        votos_list = Aportacion_votada.objects.filter(usuario=perfil)
        list_sites = last_sites()
        context = {'aportacion_list': aportacion_list, 'comentario_list': comentario_list, 'votos_list': votos_list,
                   'perfil': perfil, 'list_sites': list_sites}
        formato = request.GET.get('format')
        if formato == 'xml':
            return render(request, 'LoVisto/usuario.xml', context, content_type='text/xml')
        if formato == 'json':
            return render(request, 'LoVisto/usuario.json', context, content_type='text/json')
        return render(request, 'LoVisto/usuario.html', context)

    else:
        return render(request, 'LoVisto/error.html')

def info(request):
    if request.user.is_authenticated:
        perfil = Perfil.objects.get(usuario=request.user)
        if request.method == "POST":
            perfil = Perfil.objects.get(usuario=request.user)
            if "modo" in request.POST:
                if (perfil.modo == 'n'):
                    perfil.modo = 'o'
                else:
                    perfil.modo = 'n'
                perfil.save()
    else:
        perfil = None
    list_sites = last_sites()
    formato = request.GET.get('format')
    if formato == 'xml':
        return render(request, 'lovisto/info.xml', None, content_type='text/xml')
    if formato == 'json':
        return render(request, 'LoVisto/info.json', None, content_type='text/json')
    return render(request, 'LoVisto/info.html', {'perfil': perfil, 'list_sites': list_sites})


