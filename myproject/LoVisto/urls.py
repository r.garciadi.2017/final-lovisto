from django.urls import path

from . import views

urlpatterns = [
    path('logout', views.logout_view),
    path('registro', views.registro),
    path('todo', views.todo),
    path('usuario', views.usuario),
    path('info', views.info),
    path('<int:llave>', views.aportacion),
    path('', views.index),
]
