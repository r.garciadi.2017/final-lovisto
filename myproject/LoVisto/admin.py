from django.contrib import admin

# Register your models here.
from .models import Aportacion, Perfil, Aemet, Dia, Youtube, Wikipedia, Otro, Comentario, Aportacion_votada

admin.site.register(Aportacion)
admin.site.register(Perfil)
admin.site.register(Aemet)
admin.site.register(Dia)
admin.site.register(Youtube)
admin.site.register(Wikipedia)
admin.site.register(Otro)
admin.site.register(Comentario)
admin.site.register(Aportacion_votada)
