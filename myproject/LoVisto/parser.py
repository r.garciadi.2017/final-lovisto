from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string
from .models import Aemet, Dia, Aportacion

class Aemet(ContentHandler):
    """Clase para:
    Obtener contenido de un documento XML
    """

    def __init__(self):

        self.inRoot = False
        self.inContent = False
        self.inTemp = False
        self.inSens = False
        self.inHum = False

        self.content = ""
        self.url = ""
        self.municipio = ""
        self.provincia = ""
        self.dia = []
        self.copyright = ""
        self.temp_max = []
        self.temp_min = []
        self.sens_max = []
        self.sens_min = []
        self.hum_max = []
        self.hum_min = []

    def startElement (self, name, attrs):
        if name == 'root':
            self.inRoot = True

        elif self.inTemp:
            if name == 'maxima':
                self.inContent = True
            if name == 'minima':
                self.inContent = True
        elif self.inSens:
            if name == 'maxima':
                self.inContent = True
            if name == 'minima':
                self.inContent = True
        elif self.inHum:
            if name == 'maxima':
                self.inContent = True
            if name == 'minima':
                self.inContent = True

        elif self.inRoot:
            if name == 'enlace':
                self.inContent = True
            elif name == 'nombre':
                self.inContent = True
            elif name == 'provincia':
                self.inContent = True
            elif name == 'temperatura':
                self.inTemp = True
            elif name == 'sens_termica':
                self.inSens = True
            elif name == 'humedad_relativa':
                self.inHum = True
            elif name == 'copyright':
                self.inContent = True
            elif name == 'dia':
                self.dia.append(attrs.get('fecha'))

    def endElement (self, name):
        if name == 'root':
            self.inRoot = False
            try:
                aport = Aportacion.objects.get(url=self.url)
            except Aportacion.DoesNotExist:
                self.url = self.url.replace("https:", "http:")
                aport = Aportacion.objects.get(url=self.url)

            A = Aemet(aportacion=aport, municipio=self.municipio, provincia=self.provincia, copyright=self.copyright)
            A.save()
            for x in range(7):
                D = Dia(dia = self.dia[x], temp_max = self.temp_max[x], temp_min = self.temp_min[x], sens_max = self.sens_max[x],
                        sens_min = self.sens_min[x], hum_max = self.hum_max[x], hum_min = self.hum_min[x], aemet = A)
                D.save()

        elif self.inRoot:
            if name == 'enlace':
                self.url = self.content
                self.content = ""
                self.inContent = False
            elif name == 'nombre':
                self.municipio = self.content
                self.content = ""
                self.inContent = False
            elif name == 'provincia':
                self.provincia = self.content
                self.content = ""
                self.inContent = False
            elif name == 'temperatura':
                self.inTemp = False
            elif name == 'sens_termica':
                self.inSens = False
            elif name == 'humedad_relativa':
                self.inHum = False
            elif name == 'copyright':
                self.copyright = self.content
                self.content = ""
                self.inContent = False

            elif self.inTemp:
                if name == 'maxima':
                    self.temp_max.append(self.content)
                    self.content = ""
                    self.inContent = False
                if name == 'minima':
                    self.temp_min.append(self.content)
                    self.content = ""
                    self.inContent = False
            elif self.inSens:
                if name == 'maxima':
                    self.sens_max.append(self.content)
                    self.content = ""
                    self.inContent = False
                if name == 'minima':
                    self.sens_min.append(self.content)
                    self.content = ""
                    self.inContent = False
            elif self.inHum:
                if name == 'maxima':
                    self.hum_max.append(self.content)
                    self.content = ""
                    self.inContent = False
                if name == 'minima':
                    self.hum_min.append(self.content)
                    self.content = ""
                    self.inContent = False

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars

class AemetParser:

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = Aemet()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

from bs4 import BeautifulSoup
import urllib.request

from django.core.serializers import json


def html_parser(url):
    objeto = ""
    htmlStream = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlStream, 'html.parser')
    if ("aemet" in url):
        for div in soup.find_all('div', class_='enlace_xml inline_block'):
            objeto = "https://www.aemet.es" + div.find('a')['href']

    elif ("youtube" in url):
        objeto = soup.find("link", {"type": "application/json+oembed"})['href']

    elif ("wikipedia" in url):
        objeto = soup.find("h1", {"id": "firstHeading"}).text
        if (" " in objeto):
            objeto = objeto.replace(" ", "_")

    return objeto

from bs4 import BeautifulSoup
import urllib.request
from .models import Aportacion, Otro

def html_parser2(url):
    htmlStream = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlStream, 'html.parser')
    meta = soup.find("meta", {"property": "og:title"})
    if (meta == None):
        titulo = soup.find('title')
        if (titulo != None):
            titulo = titulo.string
        else:
            titulo = "Información extendida no disponible"
        aport = Aportacion.objects.get(url=url)
        O = Otro(aportacion=aport, titulo=titulo)
        O.save()
    else:
        titulo = meta['content']
        img = soup.find("meta", {"property": "og:image"})['content']
        aport = Aportacion.objects.get(url=url)
        O = Otro(aportacion=aport, titulo=titulo, imagen=img)
        O.save()
        
import json
import urllib.request

def img_parser(url, pag_id):

    with urllib.request.urlopen(url) as json_doc:
        json_str = json_doc.read().decode(encoding="ISO-8859-1")
        objetos = json.loads(json_str)
        img = objetos["query"]["pages"][pag_id]["thumbnail"]["source"]

    return img
    
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string
from .models import Wikipedia, Aportacion
from .parser import img_parser

class TxtHandler(ContentHandler):
    """Clase para:
    Obtener contenido de un documento XML
    """

    def __init__(self):

        self.inApi = False
        self.inContent = False

        self.content = ""
        self.texto = ""
        self.titulo = ""
        self.pageid = ""

    def startElement (self, name, attrs):
        if name == 'api':
            self.inApi = True

        elif self.inApi:
            if name == 'extract':
                self.inContent = True
            if name == 'page':
                self.titulo = attrs.get('title')
                self.pag_id = attrs.get('pageid')

    def endElement (self, name):
        if name == 'api':
            self.inApi = False
            if (" " in self.titulo):
                self.titulo = self.titulo.replace(" ", "_")
            url = "https://es.wikipedia.org/wiki/" + self.titulo
            try:
                aport = Aportacion.objects.get(url=url)
            except Aportacion.DoesNotExist:
                url = url.replace("https:", "http:")
                aport = Aportacion.objects.get(url=url)

            img = img_parser("https://es.wikipedia.org/w/api.php?action=query&titles={articulo}&prop=pageimages&format=json&pithumbsize=100".replace("{articulo}", self.titulo), self.pag_id)
            W = Wikipedia(aportacion=aport, articulo=self.titulo, texto=self.texto, imagen=img)
            W.save()

        elif self.inApi:
            if name == 'extract':
                self.texto = self.content
                self.content = ""
                self.inContent = False

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars

class TxtParser:

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = TxtHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)
        
import json
import urllib.request
from .models import Youtube, Aportacion

def ytb_js_handler(url, url_org):

    with urllib.request.urlopen(url) as json_doc:
        json_str = json_doc.read().decode(encoding="ISO-8859-1")
        objetos = json.loads(json_str)

        aport = Aportacion.objects.get(url=url_org)
        Y = Youtube(aportacion=aport, titulo=objetos['title'], autor=objetos['author_name'], video=objetos['html'])
        Y.save()
